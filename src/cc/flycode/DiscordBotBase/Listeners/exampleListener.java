package cc.flycode.DiscordBotBase.Listeners;

import cc.flycode.DiscordBotBase.Base.Bot;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

/**
 * Created by FlyCode on 18/07/2018 Package cc.flycode.DiscordBotBase.Listeners
 */

public class exampleListener extends Bot {
    public exampleListener() {
        //true = Enable / Disable the listener
        //String 1 - "Example Listener" = The name of the listener
        //String 2 - "this is my very cool description for this listener!" = The description of the listener!
        super(true,"Example Listener","this is my very cool description for this listener!");
    }
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        String message = event.getMessage().getRawContent();
        if (message.contains("hello world")) {
            event.getTextChannel().sendMessage("The Listener Works!").queue();
        }
    }
}
