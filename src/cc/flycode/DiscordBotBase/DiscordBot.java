package cc.flycode.DiscordBotBase;

import cc.flycode.DiscordBotBase.Base.RegisterManager;
import cc.flycode.DiscordBotBase.Base.Strings;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import javax.security.auth.login.LoginException;

/**
 * Created by FlyCode on 18/07/2018 Package cc.flycode.DiscordBotBase
 */

public class DiscordBot extends ListenerAdapter {
    public static void main(String[] args) throws LoginException, RateLimitedException, InterruptedException {
        RegisterManager registerManager = new RegisterManager();
        Strings strings = new Strings();
        JDA jda = new JDABuilder(AccountType.BOT).setToken(strings.DiscordBotToken).buildBlocking();
        registerManager.init(jda);
    }
}
