package cc.flycode.DiscordBotBase.Base.Utils;

import cc.flycode.DiscordBotBase.Base.RegisterManager;

/**
 * Created by FlyCode on 18/07/2018 Package cc.flycode.DiscordBotBase.Base.Utils
 */

public class Helper {
    private static String prefix = RegisterManager.Prefix;
    public static void log(String string) {
        System.out.println(prefix + " " + string);
    }
 }
