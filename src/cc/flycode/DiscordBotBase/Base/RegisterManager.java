package cc.flycode.DiscordBotBase.Base;

import cc.flycode.DiscordBotBase.Base.Utils.Helper;
import cc.flycode.DiscordBotBase.Listeners.exampleListener;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;

/**
 * Created by FlyCode on 18/07/2018 Package cc.flycode.DiscordBotBase.Base
 */

public class RegisterManager {
    private double Version = 0.1;
    public static String Prefix;
    private Strings strings = new Strings();
    private JDAUtils jdaUtils = new JDAUtils();
    private ArrayList<ListenerAdapter> listenerAdapters = new ArrayList<>();
    private ArrayList<ListenerAdapter> registered = new ArrayList<>();
    public void init(JDA jda) {
        Prefix = strings.Prefix;
        strings.version = Version;
        strings.name = jda.getSelfUser().getName();
        loadListeners();
        jdaUtils.updateGame(jda,"Hardcore game!!!11");
        jdaUtils.updateStatus(jda,JDAUtils.OnlineStatusEnums.ONLINE);
        for (ListenerAdapter la : listenerAdapters) {
            if (!registered.contains(la)) {
                registered.add(la);
                jda.addEventListener(la);
            }
        }
    }
    protected void loadListeners() {
        /**
         * Here you can add your listeners example: addListener(new MyListener());
         */
        addListener(new exampleListener());
    }
    private void addListener(Bot bot) {
        if (!listenerAdapters.contains(bot) && !registered.contains(bot) && bot.isEnabled()) {
            listenerAdapters.add(bot);
            Helper.log("New Listener registered: " + bot.getName() + " - " + bot.getDescription());
        } else if (!bot.isEnabled()) {
            Helper.log("Unable to register " + bot.getName() + " because its not enabled!");
        }
    }
}
