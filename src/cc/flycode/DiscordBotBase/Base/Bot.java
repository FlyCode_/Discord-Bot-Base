package cc.flycode.DiscordBotBase.Base;

import cc.flycode.DiscordBotBase.Base.Utils.Helper;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

/**
 * Created by FlyCode on 18/07/2018 Package cc.flycode.DiscordBotBase
 */

public class Bot extends ListenerAdapter {
    private boolean enabled;
    private String name;
    private String description;
    public Bot(boolean enabled, String name, String description) {
        this.enabled = enabled;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            Helper.log("Listener name set to: " + name);
        } else if (name == null) {
            Helper.log("Listener name is null");
        } else if (name.length() <= 0) {
            Helper.log("Listener name length cannot be equal to or below MIN:0 : MAX:" + String.valueOf(name.length()));
        } else if (name.equalsIgnoreCase(" ")) {
            Helper.log("Listener name cannot be empty");
        }
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description != null) {
            Helper.log("Listener description set to: " + description);
        } else if (description == null) {
            Helper.log("Listener description is null");
        }
        this.description = description;
    }

    public boolean isEnabled() {
        if (enabled) {
            Helper.log("Enabling: " + this.name);
        } else if (!enabled) {
            Helper.log("Unable to enable: " + this.name + " because its disabled!");
        }
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            Helper.log("Enabling: " + this.name);
        } else if (!enabled) {
            Helper.log("Disabling: " + this.name);
        }
        this.enabled = enabled;
    }
}
