Discord bot base
i made this because i was board have fun with it :)

how do i use it??????!!!!111:
1. fork or download the src
2. import it into IntelliJ (https://www.jetbrains.com/idea/)
3. write your code
4. find the class "Strings" and find DiscordBotToken replace ENTER_YOUR_DISCORD_BOT_TOKEN_HERE with your Discord bot token!
5. add the listner to the class RegisterManager.java under the loadListeners (follow the example)
6. enjoy your discord bot :P

How to run the bot:
use this if "java -jar MyBot.jar" does not work
"java -cp MyBot.jar cc.flycode.DiscordBotBase.DiscordBot"

Exporting in IntelliJ:
1. Go to "Project Settings"
2. find "Artifacts"
3. Click the + icon, select JAR and click "from modules with dependencies"
4. select the main class (cc.flycode.DiscordBotBase.DiscordBot) and leave everything else as default
5. click "Apply"
6. select the export path apply again and close
7. find build -> build artifacts -> DiscordBotBase:jar -> (Build or Rebuild, up to you)
8. run the bot using the instructions above!